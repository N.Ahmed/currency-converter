# Currency Converter

This is a simple converter that is hosted on a webserver which takes a value of a currency inputed by the user and compares it to the country of choice which will give them the current currency conversion.

## Getting Started

These instructions will help get you to host this project on a webserver online. It is possible to host it locally but for the purpose of this project we will focus on the webserver side.

### Prerequisites


These prerequisites are required to run on the webserver itself and was only tested in the Ubuntu distribution

Ubuntu

PHP (7.0 or Higher)

```
sudo apt-get update
sudo apt-get install php7.0-common

```

Apache
```
sudo apt-get update
sudo apt-get install apache

```



### Installation

First step is to clone the repository where the apache virtualhost can point too. It's preferable to have a parent directory where you can store the clone files

```
cd /var/www/your-directory-name
git clone https://gitlab.com/N.Ahmed/currency-converter

```

Next you'll want to go to the apache directory and add the site of the project, you can copy over and exisiting .conf file and work from there

```
cd /etc/apache2/sites-availible
cp 000-defaul.conf your-site.conf

```

Set your virtual host parameters to point to your project file

```
<VirtualHost *:80>
    ServerName your_server_name.com
    ServerAlias www.your_server_alias.com
    DocumentRoot /var/www/your-directory-name
    <Directory /var/www/your-directory-name>
          Allowoverride All
    </Directory>
</VirtualHost>

```
Next reload the apache 

```
sudo service apache2 reload
```

finally go to your to servername set in your virtualhost to see the contents of the Project

```
http://your_server_name.com
```

### Built With

Codeigniter 4 - PHP Framwork

### Author

Nabid Ahmed