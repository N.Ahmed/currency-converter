<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {

   	parent::__construct();

   	// load base_url
   	$this->load->helper('url');

  	$this->load->library('parser');
    }

    public function index()
    {
         $conversion_rate = 0;
	 // Check form submit or not
   	 if($this->input->post('submit') != NULL ){
 
     	     // POST data
     	     $postData = $this->input->post();

     	     // API Data
     	     $response = file_get_contents('https://api.exchangeratesapi.io/latest?base=' . $postData['country']);
     	     $rates = json_decode($response, true);
     	     $dest_country_rate = $postData['conversion_value'];
     	     $source_country_rate = $rates["rates"]["" . $postData['country_rate'] . ""];
    	     $conversion_rate = $source_country_rate * $dest_country_rate;
     }

     $data = array(
       	'conversion_rate' => $conversion_rate
     );
   
     //Pass Value
     $this->parser->parse('welcome_message', $data);
   }
}
